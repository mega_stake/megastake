#ifndef GENESIS_H
#define GENESIS_H

#include "bignum.h"

// Genesis block 0 hash

static const uint256 hashGenesisBlock("0000018901dedfccbf71f55c6c3569a5388ca3401e74ae0d1c5b234e016788ec");
static const uint256 hashGenesisBlockTestNet("0000018901dedfccbf71f55c6c3569a5388ca3401e74ae0d1c5b234e016788ec");

static const uint256 CheckPointBlock1("0000000000384c354c4c4b8ce36cf9b310a274c91ec6165a49be5e8bde56b6b6") ; //checkpoint at block 520
static const uint256 CheckPointBlock2("79e1d9a08a6fa8ed8de1bb7355bd609f963d48a3f0ffaa0eaadd915edf591f9c") ; //checkpoint at block 5000


// TODO: Move Kernel checksum in here


#endif // GENESIS_H
