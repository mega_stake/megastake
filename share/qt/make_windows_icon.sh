#!/bin/bash
# create multiresolution windows icon
ICON_DST=../../src/qt/res/icons/megastake.ico

convert ../../src/qt/res/icons/megastake-16.png ../../src/qt/res/icons/megastake-32.png ../../src/qt/res/icons/megastake-48.png ${ICON_DST}
